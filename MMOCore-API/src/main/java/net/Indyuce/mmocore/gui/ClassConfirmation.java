package net.Indyuce.mmocore.gui;

import io.lumine.mythic.lib.UtilityMethods;
import io.lumine.mythic.lib.api.item.ItemTag;
import io.lumine.mythic.lib.api.item.NBTItem;
import net.Indyuce.mmocore.MMOCore;
import net.Indyuce.mmocore.api.ConfigMessage;
import net.Indyuce.mmocore.api.SoundEvent;
import net.Indyuce.mmocore.api.event.PlayerChangeClassEvent;
import net.Indyuce.mmocore.api.player.PlayerData;
import net.Indyuce.mmocore.api.player.profess.PlayerClass;
import net.Indyuce.mmocore.api.player.profess.SavedClassInformation;
import net.Indyuce.mmocore.gui.api.EditableInventory;
import net.Indyuce.mmocore.gui.api.GeneratedInventory;
import net.Indyuce.mmocore.gui.api.InventoryClickContext;
import net.Indyuce.mmocore.gui.api.PluginInventory;
import net.Indyuce.mmocore.gui.api.item.InventoryItem;
import net.Indyuce.mmocore.gui.api.item.Placeholders;
import net.Indyuce.mmocore.gui.api.item.SimplePlaceholderItem;
import net.Indyuce.mmocore.player.ClassDataContainer;
import net.Indyuce.mmocore.skill.ClassSkill;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

public class ClassConfirmation extends EditableInventory {
    private final PlayerClass playerClass;

    public ClassConfirmation(PlayerClass playerClass, boolean isDefault) {
        super("class-confirm-" + (isDefault ? "default" : UtilityMethods.ymlName(playerClass.getId())));

        this.playerClass = playerClass;
    }

    @Override
    public InventoryItem load(String function, ConfigurationSection config) {
        if (function.equalsIgnoreCase("yes"))
            return new YesItem(config);
        if (function.equalsIgnoreCase("skill"))
            return new SkillItem(config);
        
        return new SimplePlaceholderItem(config);
    }

    public GeneratedInventory newInventory(PlayerData data, PluginInventory last, boolean setClass) {
        return newInventory(data, last, setClass, null);
    }

    public GeneratedInventory newInventory(PlayerData data, PluginInventory last, boolean setClass, @Nullable Runnable profileRunnable) {
        return new ClassConfirmationInventory(data, this, playerClass, last, setClass, profileRunnable);
    }

    public class UnlockedItem extends InventoryItem<ClassConfirmationInventory> {
        public UnlockedItem(ConfigurationSection config) {
            super(config);
        }

        @Override
        public Placeholders getPlaceholders(ClassConfirmationInventory inv, int n) {
            PlayerClass profess = inv.profess;
            ClassDataContainer info = inv.subclass ? inv.getPlayerData() : inv.getPlayerData().getClassInfo(profess);
            Placeholders holders = new Placeholders();

            final double nextLevelExp = inv.getPlayerData().getLevelUpExperience();
            final double ratio = info.getExperience() / nextLevelExp;

            StringBuilder bar = new StringBuilder("" + ChatColor.BOLD);
            int chars = (int) (ratio * 20);
            for (int j = 0; j < 20; j++)
                bar.append(j == chars ? "" + ChatColor.WHITE + ChatColor.BOLD : "").append("|");

            holders.register("percent", decimal.format(ratio * 100));
            holders.register("progress", bar.toString());
            holders.register("class", profess.getName());
            holders.register("unlocked_skills", info.mapSkillLevels().size());
            holders.register("class_skills", profess.getSkills().size());
            holders.register("next_level", "" + nextLevelExp);
            holders.register("level", info.getLevel());
            holders.register("exp", info.getExperience());
            holders.register("skill_points", info.getSkillPoints());

            return holders;
        }
    }

    public class YesItem extends SimplePlaceholderItem<ClassConfirmationInventory> {
        private final InventoryItem unlocked, locked;

        public YesItem(ConfigurationSection config) {
            super(Material.BARRIER, config);

            Validate.isTrue(config.contains("unlocked"), "Could not load 'unlocked' config");
            Validate.isTrue(config.contains("locked"), "Could not load 'locked' config");

            unlocked = new UnlockedItem(config.getConfigurationSection("unlocked"));
            locked = new InventoryItem<ClassConfirmationInventory>(config.getConfigurationSection("locked")) {

                @Override
                public Placeholders getPlaceholders(ClassConfirmationInventory inv, int n) {
                    Placeholders holders = new Placeholders();
                    holders.register("class", inv.profess.getName());
                    return holders;
                }
            };
        }

        @Override
        public ItemStack display(ClassConfirmationInventory inv, int n) {
            return inv.getPlayerData().hasSavedClass(inv.profess) ? unlocked.display(inv, n) : locked.display(inv, n);
        }
    }

    public class SkillItem extends InventoryItem<ClassConfirmationInventory> {
        public SkillItem(ConfigurationSection config) {
            super(Material.BARRIER, config);
        }

        @Override
        public boolean hasDifferentDisplay() {
            return true;
        }

        @Override
        public ItemStack display(ClassConfirmationInventory inv, int n) {
            int index = n - inv.skillSlots.get(0);
            if (index >= inv.skills.size())
                return new ItemStack(Material.AIR);

            ClassSkill skill = inv.skills.get(index);
            Placeholders holders = getPlaceholders(skill);

            List<String> lore = new ArrayList<>(getLore());
            index = lore.indexOf("{lore}");
            lore.remove(index);

            List<String> skillLore = skill.calculateLore(inv.getPlayerData());
            for (int j = 0; j < skillLore.size(); j++)
                lore.add(index + j, skillLore.get(j));
            
            for (int j = 0; j < lore.size(); j++)
                lore.set(j, ChatColor.GRAY + holders.apply(inv.getPlayer(), lore.get(j)));
            
            // Generate Item
            ItemStack item = skill.getSkill().getIcon();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(holders.apply(inv.getPlayer(), getName()));
            meta.addItemFlags(ItemFlag.values());
            meta.setLore(lore);
            item.setItemMeta(meta);

            return NBTItem.get(item).addTag(new ItemTag("skillId", skill.getSkill().getHandler().getId())).toItem();
        }

        public Placeholders getPlaceholders(ClassSkill skill) {
            Placeholders holders = new Placeholders();
            holders.register("skill", skill.getSkill().getName());
            return holders;
        }

        @Override
        public Placeholders getPlaceholders(ClassConfirmationInventory inv, int n) {
            return new Placeholders();
        }
    }

    public class ClassConfirmationInventory extends GeneratedInventory {
        private final PlayerClass profess;
        private final PluginInventory last;
        private final boolean subclass;

        // Cached information
        private final List<ClassSkill> skills;
        private final List<Integer> skillSlots;

        @Nullable
        private final Runnable profileRunnable;

        private boolean canClose;

        public ClassConfirmationInventory(PlayerData playerData, EditableInventory editable, PlayerClass profess, PluginInventory last, boolean subclass, @Nullable Runnable profileRunnable) {
            super(playerData, editable);

            this.profess = profess;
            this.last = last;
            this.subclass = subclass;
            this.profileRunnable = profileRunnable;
            this.skills = profess.getClassSkills()
                          .stream()
                          .sorted(Comparator.comparingInt(ClassSkill::getUnlockLevel))
                          .collect(Collectors.toList());
            this.skillSlots = getEditable().getByFunction("skill").getSlots();
        }

        @Override
        public void whenClicked(InventoryClickContext context, InventoryItem item) {
            if (item.getFunction().equals("back")) {
                canClose = true;
                last.open();
            } else if (item.getFunction().equals("yes")) {

                PlayerChangeClassEvent called = new PlayerChangeClassEvent(playerData, profess);
                Bukkit.getPluginManager().callEvent(called);
                if (called.isCancelled())
                    return;

                canClose = true;
                playerData.giveClassPoints(-1);
                if (subclass)
                    playerData.setClass(profess);
                else
                    (playerData.hasSavedClass(profess) ? playerData.getClassInfo(profess)
                            : new SavedClassInformation(MMOCore.plugin.playerDataManager.getDefaultData())).load(profess, playerData);
                ConfigMessage.fromKey("class-select", "class", profess.getName()).send(player);
                MMOCore.plugin.soundManager.getSound(SoundEvent.SELECT_CLASS).playTo(player);
                player.closeInventory();
                if (profileRunnable != null) profileRunnable.run();
            }
        }

        @Override
        public void open() {
            canClose = false;
            super.open();
        }

        @Override
        public void whenClosed(InventoryCloseEvent event) {
            if (profileRunnable != null && !canClose)
                Bukkit.getScheduler().runTaskLater(MMOCore.plugin, () -> open(), 2 * 20);
        }

        @Override
        public String calculateName() {
            return getName().replace("{class}", profess.getName());
        }
    }
}
